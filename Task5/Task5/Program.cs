﻿using System;
using System.Collections.Generic;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> Contacts = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Enter next contact");
                Contacts.Add(Console.ReadLine());
            }
            //Program that will search for matches in the list
            Console.Write("What name do you want to check?");
            string find = Console.ReadLine();
            foreach ( string name in Contacts)
            {
                if (Contacts.Contains(find))
                {
                    Console.WriteLine(name);
                }
            }
        }
    }
}
