﻿using System;
using System.Collections.Generic;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> Contacts = new List<Person>
            {
                new Person ("Truls", "Olsen", 98765432),
                new Person ("Henrik", "Henriksen", 92323233),
                new Person ("Frank", "Langballe", 95465787),
                new Person ("Truls", "Langemann", 98798798),
                new Person ("Kaare", "Haa", 91919393)
            };

            //Program that will search for matches in the list
            Console.Write("What name do you want to check?");
            string find = Console.ReadLine();
            Checkmatch(find, Contacts);
            
        }
        public static void Checkmatch(string find, List<Person> Contacts)
        {
           
            for (int i = 0; i < Contacts.Count; i++)
            {
                if (Contacts[i].FirstName.Contains(find))
                {
                    
                    Console.WriteLine(Contacts[i].FirstName + " " + Contacts[i].LastName);
                }
                else if (Contacts[i].LastName.Contains(find))
                {
                    
                    Console.WriteLine(Contacts[i].FirstName + " " + Contacts[i].LastName);
                }
            }
        }
    }
}
