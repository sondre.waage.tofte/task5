﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    class Person
    {
        private string firstName;
        private string lastName;
        private double phoneNumber;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public double PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public Person(string firstName, string lastName, double phoneNumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PhoneNumber = phoneNumber;
        }

    }
}
